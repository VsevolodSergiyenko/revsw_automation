/**
 * Created by Vsevolod on 11/12/2014.
 * An example configuration file
 */
var HtmlReporter = require('protractor-html-screenshot-reporter');

exports.config = {

    // The address of a running selenium server.
    seleniumAddress: 'http://localhost:4444/wd/hub',

    // Capabilities to be passed to the webdriver instance.
    capabilities: {
        'browserName': 'chrome'
    },

    // Spec patterns are relative to the current working directly when
    // protractor is called.
    specs: ['example_pages_spec.js'],

    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000
    },

    onPrepare: function() {
        // Add a screenshot reporter and store screenshots to `/protractor_report/screenshots`:
        jasmine.getEnv().addReporter(new HtmlReporter({
            baseDirectory: '/protractor_report/screenshots'
        }));
    }
};

