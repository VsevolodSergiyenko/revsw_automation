'use strict';

describe('login tests', function () {
    var timeout = 15000;
    var loginPage;
    var dashboardPage;

    var onCondition = protractor.ExpectedConditions;

    beforeEach(function () {
        browser.ignoreSynchronization = true;
        browser.driver.manage().window().maximize();
    });

    describe('user should be able to login successfully', function () {
        it('should open dashboard', function () {

            browser.get('https://portal.revsw.net/');

            loginPage = require('../pages/login.page.js');

            browser.wait(onCondition.visibilityOf(loginPage.emailField), timeout).then(function () {
                loginPage.setEmail('developer2@revsw.com');
                loginPage.setPassword('zljSgqbpDKAVI78pwXC6');
                loginPage.signInButton.click().then(function () {
                    dashboardPage = require('../pages/dashboard.page.js');
                    browser.wait(onCondition.elementToBeClickable(dashboardPage.quickLookLoaderGif), timeout).then(function () {
                        /*
                         RevSW logo is displayed
                         */
                        expect(dashboardPage.logo.isDisplayed()).toBe(true);
                        /*
                         Dashboard Tab is displayed
                         */
                        expect(dashboardPage.dashboardTab.isDisplayed()).toBe(true);
                        /*
                         Quick Look section is displayed
                         */
                        expect(dashboardPage.quickLookSection.isDisplayed()).toBe(true);
                    })
                });
            });

        });
    })
});