'use strict';

var PageObject = function () {
    /*
     static page url
     */
//    browser.get('https://portal.revsw.net/');
    /*
     Page elements
     /*
     'Email' input field
     */
    this.emailField = element(by.css('[name="email"]'));
    /*
     'Password' input field
     */
    this.passwordField = element(by.css('[name="password"]'));
    /*
     'SignIn' button
     */
    this.signInButton = element(by.id('sign_in'));

    /*
     Page methods
     /*
     Set email
     */
    this.setEmail = function (emailValue) {
        this.emailField.sendKeys(emailValue);
    };
    /*
     Set password
     */
    this.setPassword = function (passwordValue) {
        this.passwordField.sendKeys(passwordValue);
    };
    /*
     Submit login form
     */
    this.clickSignIn = function () {
        this.signInButton.click();
        return require('../pages/dashboard.page.js');
    }
};

module.exports = new PageObject();
