'use strict';

var PageObject = function () {

    /*
     Page elements
     */
    /*
     RevSW logo
     */
    this.logo = element(by.id('logo'));
    /*
     'Dashboard' tab
     */
    this.dashboardTab = element(by.id('main_men_0'));
    /*
     'Quick Look' page section
     */
    this.quickLookSection = element(by.className('quik_look_con'));
    /*
     Page loader gif
     */
    this.quickLookLoaderGif = element(by.css('#q_stats_summary .map-ldr'));

};

module.exports = new PageObject();
